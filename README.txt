INTRODUCTION
------------
Compass Rose module provides a field type called Orientation that allows to
represent the most commons orientations (North, North-East, East, South-East,
South, South-West, West and North-West).
Also provides a formatter that displays a compass rose that represents the
selected Orientation.

INSTALLATION
------------
 1.- Install http://drupal.org/project/libraries
 2.- Install Compass Rose module as you would any other Drupal module
 3.- Go to http://beneposto.pl/jqueryrotate.
 4.- Download the jQueryRotate library to the jqueryrotate folder in your
     libraries folder.
 5.- If you have downloaded the content right, the following filepath should
     exist: [path to libraries folder]/jQueryRotate/jQueryRotate.js